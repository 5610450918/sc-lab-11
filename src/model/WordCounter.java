package model;

import java.util.HashMap;

public class WordCounter {
	private String message;
	private HashMap<String, Integer> wordCount;
	public WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String ,Integer>();
	}
	public void count(){
		for(String str : message.split(" ")){
			if(wordCount.containsKey(str)==true){
				wordCount.put(str, wordCount.get(str)+1);
			}
			else{
				wordCount.put(str, 1);
			}
		}
		
	}
	public int hasWord(String word){
//		System.out.println(wordCount);
		if(wordCount.containsKey(word)==true){
			return wordCount.get(word);
		}
		return 0;
	}
	

}
