package model;

import java.util.ArrayList;

public class Refrigerator {
	private int size;
	private ArrayList<String> things;
	
	public Refrigerator(int size){
		this.size = size;
		things = new ArrayList<String>();
	}
	public void put(String stuff) throws FullException {
		if(things.size()>=size){
			throw new FullException("Refrigerator Full");
		}
//		if(things.size()<size){
//			things.add(stuff);
//		}
		things.add(stuff);
	}
	public String takeOut(String stuff){
		int index = things.indexOf(stuff);
		if(index!=-1){
			things.remove(index);
			return stuff;
		}
		return null;
	}
	public String toString(){
		String str = "";
		for(String s : things){
			str += s+"\n";
		}
		return str;
	}
	
}
