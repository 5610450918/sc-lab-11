package main;

import model.FullException;
import model.Refrigerator;

public class Main {
	public static void main(String[] args){
		Refrigerator refrigerator = new Refrigerator(3);
		try{
			refrigerator.put("coke");
			refrigerator.put("water");
			refrigerator.put("apple");
			refrigerator.put("rice");
		}
		catch(FullException e){
			System.err.println("Error: "+e.getMessage());
			
		}
		System.out.println(refrigerator.toString());
	}

}
